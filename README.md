# Natural Language Plots application server

This branch contains the python web application of the project.

## Interaction Loop

1. The user asks for a program transformation in natural language.

2. The synthesizer computes the most likely transformation conditioned on the
	 current utterance-transformation bindings.

3. If the computed transformation is not correct (i.e. is not the one the
user described), then a **disambiguation** process begins until the user
finds the desired transformation.

In the disambiguation process the user can:

- Scroll through a gallery of all possible transformations.

- Filter the set of possible transformations by indicating a region of the
	output that changes under his desired transformation.

Below are (slightly outdated) representations of the interaction loop.

![Example execution](README.gif)

![General execution flowchart](FLOWCHART.png)

# Installation

*Note: the full list of dependencies and the required versions is in the [pyproject.toml](pyproject.toml) file.*

1. Install [poetry](https://python-poetry.org/).

2. Create the virtual environment (does not modify your computer) and run the
	 server. For example, to run the general version of the web application (as
	 opposed to the user-study version):

```
git clone https://gitlab.com/da_doomer/program-by-talking.git
cd program-by-talking
git checkout functional-server
poetry install
poetry shell
python main.py --hyperparameters hyperparameters/pilot/hyperparameters.json --port 3000
```
