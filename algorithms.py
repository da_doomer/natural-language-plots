# Local imports
from bindings import Bindings
from pytfidf import tfidf
from utterances import sanitize_str

# Native imports
from typing import List
from typing import TypeVar
from typing import Callable
from collections import Counter
from collections import defaultdict
from statistics import mean


T = TypeVar('T')


def get_transform_score(
        transform: T,
        utterance: str,
        bindings: Bindings,
        accepted: List[T],
        rejected: List[T],
        transform_dist: Callable[[T, T], float],
        rejection_alpha: float = 2.0,
        ) -> float:
    t = transform
    u = sanitize_str(utterance)
    B = bindings
    A = accepted
    R = rejected
    tdist = transform_dist

    #def tsim(t1, t2): return 1/(1+tdist(t1, t2))
    def tsim(t1, t2): return int(t1 == t2)

    # Build a mapping transform -> list of words used to describe the transform
    words = defaultdict(list)
    for up, tp in B:
        words[tp].extend(up.lower().split())

    # For non-empty bindings we have:
    # p(t | u, B) = sum[tprime in B] mean_tfidf(u, tprime, T) * sim(t, tprime)
    # 
    # where T is the set of transformations in the bindings.
    #
    # For empty bindings we have:
    # p(t | u, []) = 1.0
    if len(B) > 0:
        prior = 0.0001
        for tp in words.keys():
            tfidf_scores = list()
            for word in u.split():
                tfidf_score = tfidf(word, words[tp], words.values())
                tfidf_scores.append(tfidf_score)
                #print(f"tfidf({word}, {words[tp]}, {words.values()}) = {tfidf_score}")
            if len(tfidf_scores) > 0:
                prior += mean(tfidf_scores) * tsim(t, tp)
                print(f"mean_tfidf({u}) = {mean(tfidf_scores)}")
    else:
        prior = 1.0

    # We scale raw score if we have negotiation data. The scaling factor
    # exponentially reduces the score of rejected transformations. Otherwise,
    # the scaling factor is increased by the similarity to the closest
    # accepted transform.
    def compute_scaling_factor(tp, A, R):
        # Find the number of times the transformation has been rejected
        Rcounter = Counter(R)
        if Rcounter[tp] > 0:
            return (1/2)**(rejection_alpha*Rcounter[tp])

        # If it has not been rejected, return the similarity to the
        # nearest accepted transformation.
        accepted_sim = (1.0 + max(
                map(lambda tpp: tsim(tpp, tp), A),
            )) if len(A) > 0 else 1.0
        return accepted_sim

    return prior*compute_scaling_factor(t, A, R)
