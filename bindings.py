"""Defines the binding data structures."""
# Local imports
from plots import PlotProgramTransform

# Expose bindings raw type for deserialization with jsons
BindingsRawType = list[tuple[str, PlotProgramTransform]]


class Bindings(list[tuple[str, PlotProgramTransform]]):
    @property
    def as_dict(self) -> dict[str, tuple[PlotProgramTransform, ...]]:
        return {
                u: tuple(t for u2, t in self if u2 == u)
                for u in (u2 for u2, _ in self)
            }
