"""Implements debugging and bugging."""
from __future__ import annotations

# Local imports
from plots import PlotProgram
from plots import PlotProgramTransform
import png
from bindings import Bindings
from algorithms import get_transform_score
from utterances import sanitize_str

# Native imports
import time
import random
from pathlib import Path
import json
from statistics import mean
from typing import Callable
from typing import Dict
from typing import List
from typing import TypeVar
from typing import Tuple
from collections import Counter
from collections import defaultdict

# External imports
import numpy as np
import jsons
import matplotlib.pyplot as plt


T = TypeVar('T')


class Timer:
    def __init__(self, name: str):
        self.name = name

    def start(self):
        print("{} started".format(self.name))
        self.startt = time.time()

    def stop(self):
        self.endt = time.time()
        totalt = self.endt - self.startt
        print("{} took {}s".format(self.name, round(totalt, 2)))


def log_transform(
        program: PlotProgram,
        transform: PlotProgramTransform,
        path: Path,
        ):
    # Get the pixels of the program and the transformed program
    pixels1 = program.pixels
    pixels2 = transform.transformed(program).pixels

    # Concatenate the pixels
    pixels = np.concatenate([pixels1, pixels2])

    # Write the image
    pixels = np.array(
            [[j for i in sublist for j in i] for sublist in pixels],
            dtype=np.uint8,
        )
    with open(path, "wb") as imagefile:
        png.from_array(pixels, mode="RGB").write(imagefile)
        print(f"Wrote {path}")


NearestNeighborsQuery = Dict[T, List[Tuple[T, float]]]


def nearest_neighbors(
        points: List[T],
        dist: Callable[[T, T], float],
        k: int,
        ) -> NearestNeighborsQuery:
    query: NearestNeighborsQuery = dict()
    for point in points:
        neighbors = [p for p in points if p != point]
        nearest_neighbors = sorted(neighbors, key=lambda p: dist(point, p))[:k]
        query[point] = [(p, dist(point, p)) for p in nearest_neighbors]
    return query


def log_bindings(
        bindings: Bindings,
        utterance_dist: Callable[[str, str], float],
        transform_dist: Callable[
                [PlotProgramTransform, PlotProgramTransform], float
            ],
        log_dir: Path,
        ):
    # Log the bindings themselves
    bindings_file = log_dir/"bindings.json"
    bindings_json = jsons.dump(bindings, cls=Bindings)
    bindings_str = json.dumps(bindings_json, indent=4)
    with open(bindings_file, "wt") as bf:
        bf.write(bindings_str)
        print(f"Wrote {bindings_file}")

    # Log a nearest neighbor analysis
    utterance_nearest_neighbors_file = log_dir/"utterance_nearest_neighbors.json"
    utterance_nearest_neighbors = nearest_neighbors(
            points=[u for u, _ in bindings],
            dist=utterance_dist,
            k=5,
        )
    # utterance -> [(nearestutt1, distance, transforms), ...]
    NNLog = Dict[
            str,
            Tuple[str, Tuple[Tuple[str, float, Tuple[PlotProgramTransform]]]]
        ]
    nnlog: NNLog = dict()
    for utterance in set(u for u, _ in bindings):
        nns = tuple(
                (nn, dist, bindings.as_dict[nn])
                for (nn, dist) in utterance_nearest_neighbors[utterance]
            )
        nnlog[utterance] = nns
    nns_json = jsons.dump(
            nnlog,
            cls=NNLog,
        )
    nns_str = json.dumps(
            nns_json,
            indent=4,
        )
    with open(utterance_nearest_neighbors_file, "wt") as nnf:
        nnf.write(nns_str)
        print(f"Wrote {utterance_nearest_neighbors_file}")

    # Log a word histogram
    words = " ".join(u for u, _ in bindings).split()
    word_counter = Counter(words)
    words = sorted(words, key=lambda w: word_counter[w])
    word_counter = Counter(words)
    word_histogram_file = log_dir/"word_histogram.json"
    word_counter_json = jsons.dump(word_counter, cls=Counter)
    word_counter_str = json.dumps(word_counter_json, indent=4)
    with open(word_histogram_file, "wt") as whf:
        whf.write(word_counter_str)
        print(f"Wrote {word_histogram_file}")

    # Split utterances into train and test sets
    seed = 42
    random_ = random.Random(seed)
    train_fraction = 0.7
    unrolled_bindings = tuple(
            (sanitize_str(u), t)
            for u, t in bindings
            if len(u) > 0
        )
    train_size = int(len(unrolled_bindings)*train_fraction)
    train_unrolled_bindings = random_.sample(unrolled_bindings, k=train_size)
    test_unrolled_bindings = list(
            set(unrolled_bindings) - set(train_unrolled_bindings)
        )
    test_bindings = defaultdict(list)
    for u, t in test_unrolled_bindings:
        test_bindings[u].append(t)

    # Test top-k accuracy on different train sizes
    training_set_varying_sizes = {
            i: random_.sample(
                    train_unrolled_bindings,
                    k=i,
                )
            for i in range(len(train_unrolled_bindings))
        }

    ks = [1, 3, 5, 10]
    results_table: dict[int, dict[int, float]] = defaultdict(dict)
    for subtrain_size, subtrain_unrolled_bindings \
            in training_set_varying_sizes.items():
        if subtrain_size % 10 != 0\
                and subtrain_size != max(training_set_varying_sizes.keys()):
                continue
        print(f"{subtrain_size}/{max(training_set_varying_sizes.keys())}")
        train_bindings = subtrain_unrolled_bindings
        train_transforms = {
                t for _, t in train_bindings
            }
        round_score = defaultdict(list)
        for u, ts in test_bindings.items():
            # Score all transforms for the current utterance
            scores = {tp: get_transform_score(
                    transform=tp,
                    utterance=u,
                    bindings=train_bindings,
                    accepted=list(),
                    rejected=list(),
                    utterance_dist=utterance_dist,
                    transform_dist=transform_dist,
                ) for tp in train_transforms}

            # Sort all transforms by score
            sorted_transforms = sorted(
                    train_transforms,
                    key=lambda tp: scores[tp],
                    reverse=True,
                )

            # See if each transformation is in the top-k transforms
            top_k_results = defaultdict(list)
            for t in ts:
                for k in ks:
                    top_k_transforms = sorted_transforms[:k]
                    is_in_top_k = t in top_k_transforms
                    top_k_results[k].append(int(is_in_top_k))

            # Average the results over the list of transformations
            for k, results in top_k_results.items():
                round_score[k].append(mean(results))

        # Average the results over the list of utterances
        for k, round_results in round_score.items():
            results_table[subtrain_size][k] = mean(round_results)
            print(f"top-{k} accuracy training on {subtrain_size}: {results_table[subtrain_size][k]}")

    # Plot the accuracies
    fig_path = log_dir/"accuracy.svg"
    fig, ax = plt.subplots()
    x = results_table.keys()
    for k in ks:
        label = f"top-{k}"
        y = list()
        for xi in x:
            y.append(results_table[xi][k])
        ax.plot(x, y, label=label)
    ax.set_ylabel("Accuracy")
    ax.set_xlabel("User interactions (|B|)")
    ax.set_title(f"mean(tfidfs) * sim(t, tp), {1.0 - train_fraction} for test")
    ax.legend()
    fig.savefig(fig_path)
    print(f"Wrote {fig_path}")


def log_application_log(
        application_log: web.ApplicationLog,
        log_analysis_path: Path,
        ):
    # Extract data for (user number, interactions needed) plot
    batch_interactions_n = defaultdict(list)
    batch_users = defaultdict(list)
    for round_logs in application_log.values():
        for round_log in round_logs:
            # Skip empty rounds
            if len(round_log) == 0:
                continue

            # Skip the round if the task was not completed
            if not any(
                    round_summary.task_completed
                    for round_summary, _ in round_log
                    ):
                continue

            # Save data
            userid = round_log[0][0].userid
            interactions_n = len(round_log)
            batch_number = round_log[0][0].batch_number
            batch_interactions_n[batch_number].append(interactions_n)
            batch_users[batch_number].append(userid)

    # If the application has no successful batches
    if len(batch_users.keys()) == 0:
        return

    # Plot (user number, interactions needed)
    fig, ax = plt.subplots()
    data = tuple(di for di in batch_interactions_n.values())
    labels = list()
    completed_puzzle_n = 0
    for values in batch_interactions_n.values():
        completed_puzzle_n += len(values)
        labels.append(completed_puzzle_n)
    ax.boxplot(
            data,
            labels=labels,
        )
    ax.set_ylabel("Interactions needed for puzzle success")
    ax.set_xlabel("Cumulative number of completed puzzles (x-ticks for each user batch)")
    ax.set_title("System efficiency over number of users")
    ax.legend()
    fig.savefig(log_analysis_path)
    print(f"Wrote {log_analysis_path}")
    print(f"users_vs_interactions: {tuple((x, mean(y)) for x, y in batch_interactions_n.items())}")


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Plot a log JSON")
    parser.add_argument(
            '--log_json',
            type=Path,
            required=True,
        )
    args = parser.parse_args()
    log_path = args.log_json
    log_dir = Path("experiments")/f"{time.time()}"
    log_dir.mkdir(parents=True)
    print(f"Mkdir {log_dir}")

    # Open application log
    with open(log_path, "rt") as f:
        log_json_str = "\n".join(f)
    application_log = jsons.loads(log_json_str, cls=web.ApplicationLog)
    print(f"Opened {log_path}")

    # Perform analysis
    log_analysis_path = log_dir/"users_vs_interactions.svg"
    log_application_log(application_log, log_analysis_path)
