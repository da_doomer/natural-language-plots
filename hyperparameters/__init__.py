"""Defines hyperparameter data structures."""
# Native imports
from dataclasses import dataclass
from pathlib import Path
from typing import Tuple
from typing import List


@dataclass
class Hyperparameters:
    transform_bank: Path
    bindings: Path
    starting_program: Path

    def __init__(
            self,
            transform_bank,
            bindings,
            starting_program,
            ):
        self.transform_bank = Path(transform_bank)
        self.bindings = Path(bindings)
        self.starting_program = Path(starting_program)


@dataclass
class ExperimentHyperparameters:
    bindings: Path
    transform_bank: Path
    tasks: List[Tuple[Path, Path]]
    batch_size: int

    def __init__(
            self,
            transform_bank,
            bindings,
            tasks,
            batch_size,
            ):
        self.transform_bank = Path(transform_bank)
        self.bindings = Path(bindings)
        self.tasks = [
                tuple(map(Path, task))
                for task in tasks
            ]
        self.batch_size = batch_size
