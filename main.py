"""Application start-up logic."""
# Local imports
import utterances
from plots import PlotProgramTransform
from plots import PlotProgram
from models import DimensionalityReducer
from hyperparameters import ExperimentHyperparameters
from hyperparameters import Hyperparameters
import plots
import debugging
from bindings import Bindings
from bindings import BindingsRawType
from algorithms import get_transform_score

# Native imports
import argparse
import os
import shutil
import json
from typing import List
from itertools import product
from pathlib import Path
import time
import copy
import pickle
import tempfile

# External imports
import jsons
import bottle
from bottle import run
from bottle import static_file
from bottle import response
from bottle import route
from bottle import get
from bottle import request
from bottle import post
import matplotlib
import joblib
from tqdm import tqdm
matplotlib.use('Agg')  # needed to use matplotlib in multiple threads


# Fixed parameters
program_description = "Run the incremental synthesis webapp"
webappdir = Path("public")
index_html = webappdir/"index.html"
experiment_html = webappdir/"experiment_index.html"
tmpdir = webappdir/"tmp"


def render_to_webapp(
        program: PlotProgram,
        tmpdir: Path,
        webappdir: Path,
        cache=dict()
        ) -> Path:
    """Copy a file to a location accessible by the webapp."""
    if program in cache:
        return cache[program]
    tmpfile = tempfile.NamedTemporaryFile(
        dir=tmpdir,
        suffix=".svg",
        delete=False,
    )
    webapppath = webappdir/Path(tmpfile.name).relative_to(webappdir.resolve())
    program.render(filename=webapppath)
    cache[program] = webapppath
    return webapppath


def copy_to_webapp(
        path: Path,
        tmpdir: Path,
        webappdir: Path,
        cache=dict()
        ) -> Path:
    if path in cache:
        return cache[path]
    tmpfile = tempfile.NamedTemporaryFile(
        dir=tmpdir,
        suffix=".png",
        delete=False,
    )
    shutil.copy(path, tmpfile.name)
    webapppath = webappdir/Path(tmpfile.name).relative_to(webappdir.resolve())
    cache[path] = webapppath
    return webapppath




if __name__ == '__main__':
    # Create temp dir
    parser = argparse.ArgumentParser(description=program_description)
    parser.add_argument(
            '--hyperparameters',
            type=Path,
            default=Path("hyperparameters/general/hyperparameters.json"),
        )
    parser.add_argument(
            '--port',
            help='Server socket port.',
            type=int,
            required=True,
        )
    parser.add_argument(
            '--basis_file',
            help='Output transform basis.',
            type=Path,
            default=Path("basis.json"),
        )
    parser.add_argument(
            '--dim_reducer',
            help='Output encoder.',
            type=Path,
            default=Path("reducer.joblib"),
        )
    parser.add_argument(
            '--top_transform_n',
            help='Number of transformations in the negotiations.',
            type=int,
            default=9,
        )
    parser.add_argument(
            '--reducer_out_dims',
            help='Number of output dimensions for the transform encoder.',
            type=int,
            default=8,
        )
    args = parser.parse_args()

    # Define the logging dir
    logging_dir = Path("experiments")/f"experiment_{time.time()}"
    logging_dir.mkdir(parents=True)
    print(f"Created {logging_dir}")

    # Create a temp directory
    tmpdir.mkdir(parents=True, exist_ok=True)

    # Load program basis
    with open(args.basis_file, "rt") as basis_file:
        basis_json = json.load(basis_file)
        basis = [
                PlotProgram(kwargs=kwargs)
                for kwargs in basis_json
            ]
        print("Loaded {}".format(str(args.basis_file)))

    # Build the hyperparameters
    with open(args.hyperparameters, "rt") as hyperparameters_file:
        hyperparameters = ExperimentHyperparameters(
                **json.loads("\n".join(hyperparameters_file))
            )
        print(f"Loaded {args.hyperparameters}")

    # Open bindings
    with open(hyperparameters.bindings, "rt") as bindings_file:
        bindings_json_string = "\n".join(bindings_file)
    bindings = Bindings(jsons.loads(
            bindings_json_string,
            cls=BindingsRawType,
        ))
    print(f"Loaded {hyperparameters.bindings}")

    # Build transform bank
    recompute_cache = False
    tbank = set()
    recompute_tbank = True

    # Add all the named colors >900
    for name, color in matplotlib.colors.get_named_colors_mapping().items():
        t = PlotProgramTransform("color", name)
        tbank.add(t)
    # Add all the transforms in the bindings
    for _, t in bindings:
        tbank.add(t)
    # Add some non-absolute transforms
    tbank.add(PlotProgramTransform("linewidth", 0.5, absolute=False))
    tbank.add(PlotProgramTransform("linewidth", -0.5, absolute=False))

    # Clean the tbank for the current set of allowed plot attributes
    for t in tuple(tbank):
        if t.key not in PlotProgram.default_kwargs:
            tbank.remove(t)
    tbank = sorted(tbank)

    # Open transform dimensionality reducer
    if args.dim_reducer.exists():
        dim_reducer = joblib.load(args.dim_reducer)
        print("Loaded {}".format(args.dim_reducer))
    else:
        dim_reducer = DimensionalityReducer(args.reducer_out_dims)
        dim_reducer.fit(tbank, basis)
        joblib.dump(dim_reducer, args.dim_reducer)
        print("Wrote {}".format(args.dim_reducer))

    # Build distance functions
    def transform_dist(t1, t2):
        return PlotProgramTransform.dist(t1, t2, basis, dim_reducer)

    # Cache representatives
    for t in tqdm(tbank, desc="Precomp representatives"):
        break
        t.representative(basis)
    # Cache dists
    for t1, t2 in tqdm(list(product(tbank, tbank)), desc="Precomp dists"):
        break
        PlotProgramTransform.dist(t1, t2, basis, dim_reducer)

    # Build application
    _index_html = experiment_html
    tasks = list()
    for starting_program_file, target_program_file in hyperparameters.tasks:
        with open(starting_program_file, "rt") as f:
            starting_program_str = "\n".join(f)
            starting_program_json = json.loads(starting_program_str)
            starting_program = PlotProgram(kwargs=starting_program_json)
            print(f"Loaded {starting_program_file}")
        with open(target_program_file, "rt") as f:
            target_program_str = "\n".join(f)
            target_program_json = json.loads(target_program_str)
            target_program = PlotProgram(kwargs=target_program_json)
            print(f"Loaded {target_program_file}")
        tasks.append((starting_program, target_program))

    @route("/")
    def server_static():
        return static_file("experiment_index.html", root=webappdir)

    @route("/static/<filepath:path>")
    @route("/public/<filepath:path>")
    def server_static(filepath):
        return static_file(filepath, root=webappdir)

    @post("/plot_program")
    def plot_program():
        kwargs = dict(request.json)
        program = PlotProgram(kwargs)
        return str(render_to_webapp(program, tmpdir, webappdir))

    @post("/program_code")
    def plot_program():
        kwargs = dict(request.json)
        program = PlotProgram(kwargs)
        return program.code

    def fix_request_bindings(request_bindings) -> Bindings:
        bindings_ = Bindings()
        for binding_id, pre_binding in request_bindings.items():
            transform_index = pre_binding["transform"]
            utterance = pre_binding["utterance"]
            transform = tbank[transform_index]
            binding = (utterance, transform)
            bindings_.append(binding)
        return bindings_

    @post("/transform_scores/<utterance>")
    def transform_scores(utterance: str):
        accepted_ = request.json["accepted"]
        rejected_ = request.json["rejected"]
        bindings = fix_request_bindings(request.json["bindings"])
        accepted = [tbank[i] for i in accepted_]
        rejected = [tbank[i] for i in rejected_]
        scores = [
                get_transform_score(
                    transform=tbank[i],
                    utterance=utterance,
                    bindings=bindings,
                    accepted=accepted,
                    rejected=rejected,
                    transform_dist=transform_dist,
                )
                for i in range(len(tbank))
            ]
        return dict(enumerate(scores))

    @get("/puzzle_number")
    def puzzle_number():
        return str(len(tasks))

    @get("/puzzles/<puzzle_id:int>/starting_program")
    def starting_program(puzzle_id: int):
        return tasks[puzzle_id][0].kwargs

    @get("/puzzles/<puzzle_id:int>/target_program")
    def target_program(puzzle_id: int):
        return tasks[puzzle_id][1].kwargs

    @post("/transformed_program/<transform_index:int>")
    def transformed_program(transform_index: int):
        kwargs = dict(request.json)
        program = PlotProgram(kwargs)
        transform = tbank[transform_index]
        new_program = transform.transformed(program)
        return new_program.kwargs

    @get("/transform_code/<transform_index:int>")
    def transform_code(transform_index: int):
        transform = tbank[transform_index]
        return transform.code

    @get("/transform_number")
    def transform_number():
        return str(len(tbank))

    @post("/program_equality")
    def program_equality():
        json_ = request.json
        if isinstance(json_, str):
            json_ = json.loads(json_)
        kwargs1 = json_["program1"]
        kwargs2 = json_["program2"]
        if isinstance(kwargs1, str):
            kwargs1 = json.loads(kwargs1)
        if isinstance(kwargs2, str):
            kwargs2 = json.loads(kwargs2)
        program1 = PlotProgram(kwargs1)
        program2 = PlotProgram(kwargs2)
        return str(int(program1.pixel_equals(program2)))

    bottle.debug(True)
    run(port=args.port)

    # Start server
    print("Cleaning up")
    shutil.rmtree(tmpdir)
