# Native imports
from typing import List

# Local imports
from plots import PlotProgram
from plots import PlotProgramTransform

# External imports
from tqdm import tqdm
from umap import UMAP as Reducer
reducer_args = {}
#from sklearn.manifold import Isomap as Reducer
#reducer_args = {'n_jobs': -1}
#from sklearn.manifold import LocallyLinearEmbedding as Reducer
#reducer_args = {'n_jobs': -1}


class DimensionalityReducer:
    def __init__(self, dims: int):
        self.reducer = None
        self.dims = dims

    def __call__(self, x):
        if self.reducer is None:
            raise Exception("This model has not been trained.")
        return self.reducer.transform(x.flatten().reshape(1, -1))[0]

    def fit(self, transforms: List[PlotProgramTransform], basis: List[PlotProgram]):
        print("Building diff dimensionality reductor. This may take a while")
        X = list()
        for transform in transforms:
            X.append(transform.representative(basis).flatten())
        reducer = Reducer(n_components=self.dims, **reducer_args)
        reducer.fit(X)
        self.reducer = reducer
