# Local imports
import png

# Native imports
from typing import Union
from typing import Tuple
from typing import Dict
from typing import List
import tempfile
from pathlib import Path
import copy
import random
from dataclasses import dataclass
import shutil
import threading
from abc import ABC
from abc import abstractmethod
import concurrent.futures
import statistics

# External imports
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

# Constants
data = [1, 4, 9, 2, 5]  # data for plotting

# https://matplotlib.org/3.2.1/gallery/lines_bars_and_markers/marker_reference.html
matplotlib_markers = [m for m, func in matplotlib.lines.Line2D.markers.items()
                      if func != 'nothing' and m not in
                      matplotlib.lines.Line2D.filled_markers]

# https://matplotlib.org/3.2.1/gallery/lines_bars_and_markers/linestyles.html
parametrized_linestyles_examples = [
        (0, (1, 10)),
        (0, (1, 1)),
        (0, (1, 1)),

        (0, (5, 10)),
        (0, (5, 5)),
        (0, (5, 1)),

        (0, (3, 10, 1, 10)),
        (0, (3, 5, 1, 5)),
        (0, (3, 1, 1, 1)),

        (0, (3, 5, 1, 5, 1, 5)),
        (0, (3, 10, 1, 10, 1, 10)),
        (0, (3, 1, 1, 1, 1, 1)),
        ]
matplotlib_linestyles = ['-', ':', '--', '-.'] #+ parametrized_linestyles_examples


class Type(ABC):
    def sample(self, *args, **kwargs):
        return self.rawval_to_val(self.sample_rawval(*args, **kwargs))

    @abstractmethod
    def sample_rawval(self):
        pass

    @abstractmethod
    def rawval_to_val(self, rawval):
        pass

    @property
    @abstractmethod
    def rank(self):
        """Number of values needed to parametrize a distribution of this
        type."""
        pass


class TypeFloat(Type):
    def __init__(self, a: float, b: float):
        self.range = (a, b)

    def sample_rawval(self, mu: float = None, sigma: float = None) -> float:
        """A normal distribution with the given parameters will be interpolated
        to represent the mean of a probability density function from [a, b]."""
        if mu is None:
            return random.random()
        elif sigma is not None:
            rawval = statistics.NormalDist(mu, sigma).samples(1)[0]
            rawval = max(0.0, min(1.0, rawval))
            return rawval
        raise Exception("Invalid arguments {}".format(str((mu, sigma))))

    def rawval_to_val(self, rawval: float):
        a, b = self.range
        return (b-a)*rawval + a

    @property
    def rank(self):
        return 2


class TypeRGB(TypeFloat):
    def __init__(self):
        super().__init__(0.5, 0.5)

    @staticmethod
    def rgb_to_string(r: float, g: float, b: float):
        return matplotlib.colors.rgb2hex((r, g, b))

    def rawval_to_val(self, rawval: float):
        return TypeRGB.rgb_to_string(*matplotlib.cm.rainbow(rawval)[:3])

    @property
    def rank(self):
        return 2


class TypeCategorical(Type):
    def __init__(self, choices: list):
        self.choices = choices

    def sample_rawval(self, *weights) -> int:
        if len(weights) == 0:
            weights = tuple([1.0 for _ in self.choices])
        return random.choices(range(len(self.choices)), weights=weights)[0]

    def rawval_to_val(self, rawval: int):
        return self.choices[rawval]

    @property
    def rank(self):
        return len(self.choices)


class PlotProgram:
    kwargs_types: Dict[str, Type] = {
            'color': TypeRGB(),
            'linestyle': TypeCategorical(matplotlib_linestyles),
            'linewidth': TypeFloat(1.0, 5.0),
            'marker': TypeCategorical(matplotlib_markers),
            #'markeredgecolor': TypeRGB(),
            #'markerfacecolor': TypeRGB(),
            #'markeredgewidth': TypeFloat(1.0, 5.0),
            'markersize': TypeFloat(10.0, 50.0),
        }
    default_kwargs: Dict[str, Union[str, float]] = {
            'color': 'blue',
            'linestyle': '-',
            'linewidth': 1.0,
            'marker': '',
            #'markeredgecolor': 'blue',
            #'markerfacecolor': 'blue',
            #'markeredgewidth': 1.0,
            'markersize': 10.0,
        }
    # cache renders here (kwargs, suffix) -> path:
    lock = threading.Lock()
    dpi = 40
    xinches = 6.4
    yinches = 4.8
    xpixels = int(xinches*dpi)
    ypixels = int(yinches*dpi)
    division_n = 10

    def __init__(
            self,
            kwargs: Dict[str, Union[str, float]] = dict(),
            ) -> None:
        _kwargs = dict()
        for key, val in PlotProgram.default_kwargs.items():
            if key in kwargs:
                _kwargs[key] = kwargs[key]
            else:
                _kwargs[key] = val
        self.kwargs = _kwargs

    @property
    def pixels(self) -> np.ndarray:
        return PlotProgram.kwargs_pixels(self.kwargs)

    def render(
            self,
            suffix: str = ".svg",
            filename: Union[Path, None] = None
            ) -> Path:
        """Returns the Path with an SVG rendering of the program"""
        if filename is not None:
            suffix = Path(filename).suffix
        _filename = PlotProgram.render_kwargs(self.kwargs, suffix)
        if filename is not None:
            shutil.copy(_filename, filename)
            return filename
        return _filename

    @property
    def code(self) -> str:
        def format_arg(key, value):
            if PlotProgram.default_kwargs[key] == value:
                return ""
            if isinstance(value, str):
                value = '"{}"'.format(value)
            if isinstance(value, float):
                value = round(value, 3)
            else:
                value = str(value)
            return "{}={}".format(key, value)
        arguments = ["x", "y"] + [format_arg(key, value) for key, value in
                                  self.kwargs.items()]
        arguments = [a for a in arguments if len(a) > 0]
        arguments_str = ", ".join(arguments)
        command_str = "ax.plot({})".format(arguments_str)
        return command_str

    def __str__(self) -> str:
        return self.code

    def __eq__(self, other) -> bool:
        if isinstance(other, PlotProgram):
            return self.kwargs == other.kwargs
        return False

    def __hash__(self):
        return hash(tuple(sorted(self.kwargs.items())))

    def pixel_equals(self, other):
        if not isinstance(other, PlotProgram):
            return False
        return set(PlotProgram.diff_pixels(self, other).flatten()) == set([0])

    @staticmethod
    def kwargs_fig(kwargs, dpi=None, figsize=None):
        if dpi is None:
            dpi = PlotProgram.dpi
        if figsize is None:
            figsize = (PlotProgram.xinches, PlotProgram.yinches)
        fig, ax = plt.subplots(dpi=dpi, figsize=figsize)
        x = list(range(len(data)))
        ax.plot(x, data, **kwargs)
        return fig

    @staticmethod
    def render_kwargs(kwargs, suffix, dpi=None, figsize=None) -> Path:
        filename = Path(
                tempfile.NamedTemporaryFile(suffix=suffix).name
            )
        fig = PlotProgram.kwargs_fig(kwargs, dpi=dpi, figsize=figsize)
        fig.savefig(str(filename))
        plt.close(fig)
        return Path(filename)

    @staticmethod
    def kwargs_pixels(kwargs, dpi=None, figsize=None) -> np.ndarray:
        # Get the underlying PNG array
        #https://stackoverflow.com/questions/35355930/matplotlib-figure-to-image-as-a-numpy-array
        fig = PlotProgram.kwargs_fig(kwargs, dpi=dpi, figsize=figsize)
        width, height = fig.get_size_inches() * fig.get_dpi()
        width = int(width)
        height = int(height)
        canvas = FigureCanvas(fig)
        canvas.draw()
        pixels = np.fromstring(canvas.tostring_rgb(), dtype='uint8')
        pixels = pixels.reshape((height, width, 3))
        plt.close(fig)
        return pixels

    @staticmethod
    def init_random() -> 'PlotProgram':
        program = PlotProgram()
        for key, type_ in PlotProgram.kwargs_types.items():
            program.kwargs[key] = type_.sample()
        return program

    @staticmethod
    def diff_pixels(
            program1: 'PlotProgram',
            program2: 'PlotProgram',
            dpi=None,
            figsize=None,
            ) -> np.ndarray:
        # Returns a pixels-level diff of the two programs
        pixels1 = PlotProgram.kwargs_pixels(
                program1.kwargs,
                dpi=dpi,
                figsize=figsize,
                )
        pixels2 = PlotProgram.kwargs_pixels(
                program2.kwargs,
                dpi=dpi,
                figsize=figsize,
                )
        return pixels1 - pixels2

    @staticmethod
    def diff(program1: 'PlotProgram', program2: 'PlotProgram') -> Path:
        pixel_diff = PlotProgram.diff_pixels(program1, program2)
        pixels = pixel_diff[:, :, :3]  # Remove alpha dimension
        _min = min(pixels.flatten())
        pixels = pixels-_min
        _max = max(pixels.flatten())
        if _max != 0:
            pixels = pixels/_max*255
        for index, val in np.ndenumerate(pixels):
            pixels[index] = round(val)
        newimagefile = tempfile.NamedTemporaryFile(suffix=".png")
        pixels = [[j for i in sublist for j in i] for sublist in pixels]
        pixels = np.array(pixels, dtype=np.uint8)
        png.from_array(pixels, mode="RGB").write(newimagefile)
        return Path(newimagefile.name)


def cosine(a: np.ndarray, b: np.ndarray) -> float:
    eps = 0.001
    return a.dot(b)/(eps+np.linalg.norm(a)*np.linalg.norm(b))


transform_executor = concurrent.futures.ProcessPoolExecutor()
transform_representative_cache = dict()
transform_representative_cache_lock = threading.Lock()
transform_dist_cache = dict()
transform_dist_cache_lock = threading.Lock()


@dataclass(frozen=True)
class PlotProgramTransform:
    key: str
    value: Union[float, str]
    absolute: bool = True

    def transformed(self, program_original: PlotProgram):
        program = copy.deepcopy(program_original)
        if self.absolute or not isinstance(program.kwargs[self.key], float):
            program.kwargs[self.key] = self.value
        else:
            program.kwargs[self.key] += self.value
        return program

    def __eq__(self, other):
        if isinstance(other, PlotProgramTransform):
            return self.code == other.code
        return False

    def __hash__(self):
        return hash(self.code)

    @property
    def code(self) -> str:
        if isinstance(self.value, str):
            valuestr = '"{}"'.format(self.value)
        elif isinstance(self.value, float):
            valuestr = str(round(self.value, 2))
        else:
            valuestr = str(self.value)
        if self.absolute:
            change_line = "{} = {}".format(str(self.key), valuestr)
        else:
            if self.value < 0.0:
                change_line = "{} -= {}".format(str(self.key), valuestr)
            else:
                change_line = "{} += {}".format(str(self.key), valuestr)
        return change_line

    def __str__(self) -> str:
        return self.code

    def representative(
            self,
            basis: List[PlotProgram],
            ) -> np.ndarray:
        # Assumes constant basis
        if self.code in transform_representative_cache.keys():
            return transform_representative_cache[self.code]
        futures = list()
        def faux(p): return p.pixels
        for i in range(len(basis)):
            program = basis[i]
            transformed_program = self.transformed(program)
            future = transform_executor.submit(
                    PlotProgram.diff_pixels,
                    program,
                    transformed_program,
                    dpi=10,
                    figsize=(6.4, 4.8),
                )
            futures.append(future)
        vecs = [future.result() for future in futures]
        representative = np.concatenate(vecs, axis=0)
        with transform_representative_cache_lock:
            transform_representative_cache[self.code] = representative
        return representative

    @staticmethod
    def dist(t1, t2, basis: List[PlotProgram], dim_reducer):
        cache_key = "".join(sorted((t1.code, t2.code)))
        if cache_key in transform_dist_cache.keys():
            return transform_dist_cache[cache_key]
        t1key = t1.code
        t2key = t2.code
        if t1key in transform_dist_cache.keys():
            v1 = transform_dist_cache[t1key]
        else:
            v1 = dim_reducer(t1.representative(basis))
            transform_dist_cache[t1key] = v1
        if t2key in transform_dist_cache.keys():
            v2 = transform_dist_cache[t2key]
        else:
            v2 = dim_reducer(t2.representative(basis))
            transform_dist_cache[t2key] = v2
        dist = np.linalg.norm(v1-v2)
        with transform_dist_cache_lock:
            transform_dist_cache[cache_key] = dist
        return dist

    def __lt__(self, other):
        return self.code < other.code


class PlotProgramTransformGrammar:
    """Probabilistic grammar for the DSL of transformation programs"""
    terminal_keys = [t for t in PlotProgram.kwargs_types.keys()]
    terminals = [PlotProgram.kwargs_types[k] for k in terminal_keys]
    nonterminals = [TypeCategorical(terminals)]
    terminals_ranks = [t.rank for t in terminals]
    nonterminals_ranks = [t.rank for t in nonterminals]
    symbols = nonterminals + terminals
    symbol_ranks = [t.rank for t in symbols]

    def __init__(self, parameters: List[List[float]] = None):
        # Parameters is of length n_symbols, each entry containing the
        # parameters of the corresponding symbol.
        self.parameters = parameters

    def sample_with_ast_annotation(
            self
            ) -> Tuple[PlotProgramTransform, List[List[float]]]:
        """This only works for the simple case of the
        PlotProgramTransformGrammar."""
        # Sample transform
        population = PlotProgramTransformGrammar.terminal_keys
        if self.parameters is None:  # Default random
            weights = [1.0 for _ in population]
        else:
            weights = self.parameters[0]
        key_index = random.choices(
                range(len(population)), weights=weights
            )[0]
        key = population[key_index]
        type_ = PlotProgram.kwargs_types[key]
        if self.parameters is None:  # Default random
            rawval = type_.sample_rawval()
        else:
            rawval = type_.sample_rawval(*self.parameters[key_index+1])
        val = type_.rawval_to_val(rawval)
        transform = PlotProgramTransform(key, val)

        # Create AST annotation
        ast_annotation = [[0.0 for _ in range(rank)]
                          for rank in PlotProgramTransformGrammar.symbol_ranks]
        ast_annotation[0][key_index] = 1.0
        if isinstance(
                PlotProgramTransformGrammar.symbols[key_index+1],
                TypeCategorical
                ):
            ast_annotation[key_index+1][rawval] = 1.0
        else:
            ast_annotation[key_index+1] = [rawval, 0.0]
        return transform, ast_annotation

    def sample(self) -> PlotProgramTransform:
        transform, ast_annotation = self.sample_with_ast_annotation()
        return transform
