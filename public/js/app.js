/**
 * This file implements the following:
 *
 *  - application logic
 *  - UI buttons
 *
 * each of those should be in its own separate file. Will move the code later.
 **/

const plot_program_endpoint = "/plot_program";
const transformed_program_endpoint = "/transformed_program";
const puzzles_endpoint = "/puzzles";
const transform_code_endpoint = "/transform_code";
const transform_number_endpoint = "/transform_number";
const puzzle_number_endpoint = "/puzzle_number";
const program_code_endpoint = "/program_code";
const transform_score_endpoint = "/transform_scores";
const program_equality_endpoint = "/program_equality";
var fake1_passed = false;
var fake2_passed = false;
var project_name_str = "FUN PROJECT";
var remaining_tasks = 0; /* Tasks is short for server requests. */
var selected_images = [];
var images_per_row = 3;
var max_suggestions = 10;
var resample_button_more_text = "More like these";
var resample_button_discard_text = "None of these";
var puzzle_number = 0;
var QUERY_STRING_USER_ID_PARAM = "PROLIFIC_PID";
var SURVEY_CODE = "90DBEA1D";
var user_id = "testing_user";
let transform_number = 0;
let transform_codes = [];
var starting_programs = [];
var target_programs = [];
var USER_UTTERANCE = "USER_UTTERANCE";
var TRANSFORM_SORT = "SERVER_TRANSFORM_SORT";
var ACCEPT_FIRST_TRANSFORM = "ACCEPT_FIRST_TRANSFORM";
var THIS_IS_IT_CLICK = "THIS_IS_IT_GALLERY_CLICK";

var transformations_applied_in_current_puzzle = 0;
var negotiation_step = 0;
var solved_puzzles = 0;
var scoring_queries_in_current_puzzle = 0;
// This data structure holds most of what goes into updating the UI.
var state = {
	last_utterance: "",
	accepted_transforms: [],
	rejected_transforms: [],
	slide_i: 0,
	current_state: [],
	target_state: [],
	current_state_img: "",
	target_state_img: "",
	top_states: [],
	top_states_imgs: [],
	sorted_trasforms: [],
	sorted_transforms_scores: [],
	sorted_transforms_codes: [],
	codeline: "",
	summary: "",
	_alert: ""
};

function get_local_bindings_size() {
	return Object.keys(local_bindings).length;
}

function get_user_id() {
	const urlParams = new URLSearchParams(window.location.search);
	if(!urlParams.has(QUERY_STRING_USER_ID_PARAM))
		return user_id;
	return urlParams.get(QUERY_STRING_USER_ID_PARAM);
}

// Set up the hashCode function for String objects.
// https://stackoverflow.com/a/7616484
String.prototype.hashCode = function() {
	var hash = 0, i, chr;
	if (this.length === 0) return hash;
	for (i = 0; i < this.length; i++) {
		chr   = this.charCodeAt(i);
		hash  = ((hash << 5) - hash) + chr;
		hash |= 0; // Convert to 32bit integer
	}
	return hash;
};


// Shuffle using seed. Credit:
// https://github.com/yixizhang/seed-shuffle/blob/master/index.js
function shuffle(array, seed) {
	let currentIndex = array.length, temporaryValue, randomIndex;
	seed = seed || 1;
	let random = function() {
		var x = Math.sin(seed++) * 10000;
		return x - Math.floor(x);
	};
	// While there remain elements to shuffle...
	while (0 !== currentIndex) {
		// Pick a remaining element...
		randomIndex = Math.floor(random() * currentIndex);
		currentIndex -= 1;
		// And swap it with the current element.
		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}
	return array;
}


function get_current_puzzle_id() {
	let puzzle_indexes = [];
	for(let i = 0; i < puzzle_number; i++)
	{
		puzzle_indexes.push(i);
	}
	seed = user_id.hashCode();
	puzzle_indexes = shuffle(puzzle_indexes, seed);
	return puzzle_indexes[solved_puzzles];
}

function with_puzzle_number(callback) {
	increase_remaining_tasks();
	let request = new XMLHttpRequest();
	request.open("GET", puzzle_number_endpoint, true);
	request.onload = function () {
		decrease_remaining_tasks();
		callback(parseInt(request.response));
	};
	request.send();
}

function with_program_code(attributes, callback) {
	increase_remaining_tasks();
	let data = attributes;
	let request = new XMLHttpRequest();
	request.open("POST", program_code_endpoint, true);
	request.setRequestHeader("Content-Type", "application/json");
	request.onreadystatechange = function() {
		if(this.readyState === XMLHttpRequest.DONE && this.status === 200) {
			decrease_remaining_tasks();
			callback(request.responseText);
		}
	};
	request.send(data);
}

function with_starting_program(puzzle_id, callback) {
	increase_remaining_tasks();
	let endpoint = puzzles_endpoint + "/" + puzzle_id + "/starting_program";
	let request = new XMLHttpRequest();
	request.open("GET", endpoint, true);
	request.onload = function () {
		decrease_remaining_tasks();
		callback(request.response);
	};
	request.send();
}

function with_target_program(puzzle_id, callback) {
	increase_remaining_tasks();
	let endpoint = puzzles_endpoint + "/" + puzzle_id + "/target_program";
	let request = new XMLHttpRequest();
	request.open("GET", endpoint, true);
	request.onload = function () {
		decrease_remaining_tasks();
		callback(request.response);
	};
	request.send();
}

function with_starting_programs(puzzle_number_, callback) {
	let starting_programs_ = [];
	let done_n = 0;
	for(let i = 0; i < puzzle_number_; i++)
	{
		starting_programs_.push([]);
		let j = i + 0;
		// Javascript semantics guarantee this block will not be pre-empted.
		with_starting_program(j, (starting_program) => {
			starting_programs_[j] = starting_program;
			done_n = done_n + 1;
			if(done_n == puzzle_number_)
				callback(starting_programs_);
		});
	}
}

function with_target_programs(puzzle_number_, callback) {
	let target_programs_ = [];
	let done_n = 0;
	for(let i = 0; i < puzzle_number_; i++)
	{
		target_programs_.push([]);
		let j = i + 0;
		// Javascript semantics guarantee this block will not be pre-empted.
		with_target_program(j, (target_program) => {
			target_programs_[j] = target_program;
			done_n = done_n + 1;
			if(done_n == puzzle_number_)
				callback(target_programs_);
		});
	}
}

function get_starting_program(puzzle_id) {
	return starting_programs[puzzle_id];
}

function get_target_program(puzzle_id) {
	return target_programs[puzzle_id];
}

function with_transformed_program(attributes, transform_index, callback) {
	let data = attributes;
	let endpoint = transformed_program_endpoint + "/" + transform_index
	let request = new XMLHttpRequest();
	increase_remaining_tasks();
	request.open("POST", endpoint, true);
	request.setRequestHeader("Content-Type", "application/json");
	request.onreadystatechange = function() {
		if(this.readyState === XMLHttpRequest.DONE && this.status === 200) {
			decrease_remaining_tasks();
			callback(request.response);
		}
	};
	request.send(data);
}

function with_transform_scores(utterance, accepted, rejected, callback) {
	increase_remaining_tasks();
	let data = {
		"accepted": accepted,
		"rejected": rejected,
		"bindings": local_bindings,
	};
	let request = new XMLHttpRequest();
	let endpoint = transform_score_endpoint + "/" + utterance;
	request.open("POST", endpoint, true);
	request.setRequestHeader("Content-Type", "application/json");
	request.onreadystatechange = function() {
		if(this.readyState === XMLHttpRequest.DONE && this.status === 200) {
			decrease_remaining_tasks();
			scores = JSON.parse(request.response);
			let transform_scores = [];
			for(let i = 0; i < transform_number; i++)
			{
				transform_scores.push(scores[i.toString()]);
			}
			callback(transform_scores);
		}
	};
	request.send(JSON.stringify(data));
}

function with_transform_code(transform_index, callback) {
	increase_remaining_tasks();
	let endpoint = transform_code_endpoint + "/" + transform_index
	let request = new XMLHttpRequest();
	request.open("GET", endpoint, true);
	request.onload = function() {
		decrease_remaining_tasks();
		callback(request.responseText);
	};
	request.send();
}

function with_transform_codes(transform_number_, callback) {
	let transform_codes_ = [];
	let added_codes = 0;
	for(let i = 0; i < transform_number_; i++)
	{
		transform_codes_.push("");
		let j = i + 0; // Make a copy of the variable for the callback
		with_transform_code(j, (transform_code_) => {
			// Javascript semantics guarantee function execution is atomic unless
			// we call a different function. Thus, this zone is mutually-exclusive
			// zone for the callbacks.
			transform_codes_[j] = transform_code_;
			added_codes = added_codes + 1;
			if(added_codes == transform_number_)
				callback(transform_codes_);
		});
	}
}

function with_transform_number(callback) {
	increase_remaining_tasks();
	let request = new XMLHttpRequest();
	request.open("GET", transform_number_endpoint, true);
	request.onload = function() {
		decrease_remaining_tasks();
		callback(parseInt(request.response));
	};
	request.send();
}

function with_program_image_src(attributes, callback) {
	increase_remaining_tasks();
	let data = attributes;
	let request = new XMLHttpRequest();
	request.open("POST", plot_program_endpoint, true);
	request.setRequestHeader("Content-Type", "application/json");
	request.onreadystatechange = function() {
		if(this.readyState === XMLHttpRequest.DONE && this.status === 200) {
			decrease_remaining_tasks();
			callback(request.responseText);
		}
	};
	request.send(data);
}

function with_program_equals(attributes1, attributes2, callback) {
	increase_remaining_tasks();
	let data = {"program1": attributes1, "program2": attributes2};
	data = JSON.stringify(data);
	let request = new XMLHttpRequest();
	request.open("POST", program_equality_endpoint, true);
	request.setRequestHeader("Content-Type", "application/json");
	request.onreadystatechange = function() {
		if(this.readyState === XMLHttpRequest.DONE && this.status === 200) {
			decrease_remaining_tasks();
			equals_int = parseInt(request.responseText);
			if(equals_int == 1)
				callback(true);
			else
				callback(false);
		}
	};
	request.send(data);
}

function sort_state_inplace_and_update_content(state) {
	// Sort transforms from high to low
	scoring_queries_in_current_puzzle += 1;
	with_transform_scores(
		state.last_utterance,
		state.accepted_transforms,
		state.rejected_transforms,
		(transform_scores) => {
			let transforms = [];
			for(let i = 0; i < transform_number; i++)
			{
				transforms.push(i);
			}
			transforms.sort(
				(i, j) => transform_scores[j] - transform_scores[i]
			);

			// Change the state in-place
			state.sorted_transforms = transforms;
			state.sorted_transforms_scores = [];
			state.sorted_transforms_codes = [];
			state.top_states = [];
			state.top_states_imgs = [];
			let done_ip = 0;
			for(let ip = 0; ip < max_suggestions; ip++)
			{
				let i = state.sorted_transforms[ip];
				let transform_code = transform_codes[i];
				state.sorted_transforms_scores.push(transform_scores[i]);
				state.sorted_transforms_codes.push(transform_code);
				state.top_states.push([]);
				state.top_states_imgs.push("");
				// Javascript semantics guarantee these code-blocks are not
				// preempted (callback execution is done to completion).
				with_transformed_program(state.current_state, i, (transformed_program) => {
				with_program_image_src(transformed_program, (image_src) => {
					state.top_states[ip] = transformed_program;
					state.top_states_imgs[ip] = image_src;
					done_ip = done_ip + 1;
					if(done_ip  == max_suggestions)
					{
						// We got all program images. Log this sort and update content.
						add_to_csv(
							user_id,
							solved_puzzles,
							get_current_puzzle_id(),
							transformations_applied_in_current_puzzle,
							state.last_utterance,
							negotiation_step,
							state.sorted_transforms,
							get_local_bindings_size(),
							TRANSFORM_SORT,
							state.accepted_transforms,
							state.rejected_transforms
						);
						updateContent(state);
					}
				});});
			}
		}
	);
}

function increase_remaining_tasks()
{
	remaining_tasks++;
	update_bar();
}

function decrease_remaining_tasks()
{
	remaining_tasks--;
	update_bar();
}

function update_bar()
{
	progressbar = document.getElementById("progress-bar");
	progressbar.value = 100-(remaining_tasks/(remaining_tasks+30))*100;
	progressbar_div = document.getElementById("progress-bar-div");
	if(progressbar.value == 100)
		progressbar_div.style.visibility = "hidden";
	else
		progressbar_div.style.visibility = "visible";
}

function is_current_program_slide(slide_i)
{
	return slide_i == 2;
}

function is_top_program_slide(slide_i)
{
	return slide_i == 3;
}

function is_disambiguation_slide(slide_i)
{
	return slide_i == 4 || slide_i == 5;
}

function is_summary_slide(slide_i)
{
	return slide_i == 6;
}

function get_selected_images()
{
	let selected = [];
	let selected_images = document.getElementsByClassName("image-selected");
	for(const image of selected_images)
		selected.push(image.getAttribute("id"));
	return selected;
}

function get_unselected_images()
{
	let unselected = [];
	let unselected_images = document.getElementsByClassName("image-unselected");
	for(const image of unselected_images)
		unselected.push(image.getAttribute("id"));
	return unselected;
}

function update_gallery_state()
{
	// Disable top 1 button
	let top1button = document.getElementById("top_1_button");
	top1button.disabled = get_selected_images().length != 1;

	// Update resample button text
	let resamplebutton = document.getElementById("resample_button");
	if(get_selected_images().length != 0)
		resamplebutton.value = resample_button_more_text;
	else
		resamplebutton.value = resample_button_discard_text;
}

function code_to_html(code)
{
	let pre = document.createElement("pre");
	let _code = document.createElement("code");
	_code.classList.add("python");
	_code.classList.add("codeline");
	_code.innerHTML = code;
	pre.appendChild(_code);
	return pre;
}

function updateContent(state)
{
	// Build a response to simulate the server.
	var response = {
		accepted_transforms: [],
		rejected_transforms: [],
		slide_i: state.slide_i,
		current_state: state.current_state_img,
		target_state: state.target_state_img,
		codeline: state.codeline,
		top_states: state.top_states_imgs.slice(0, max_suggestions),
		top_states_changes: state.sorted_transforms_codes.slice(0, max_suggestions),
		top_states_scores: state.sorted_transforms_scores.slice(0, max_suggestions),
		summary: state.summary,
		_alert: ""
	};

	// If the synthesizer is before the second fake slide we reset the
	// passed state
	if(response.slide_i <= 1)
		fake2_passed = false;

	// +1 because the slideshow enumerates slides starting on 1
	// +1 because the first slide are the instructions
	let slide_i = response.slide_i + 1 + 1;
	// +1 because our 4th slide is fake (does not have a synthesizer state
	// equivalent)
	if(response.slide_i > 1 && fake2_passed)
		slide_i = slide_i + 1;

	// Set the current images
	let current_images = document.getElementsByClassName("current-state");
	for(let i = 0; i < current_images.length; i++)
		current_images[i].setAttribute("src", response.current_state);
	let target_images = document.getElementsByClassName("target-state");
	for(let i = 0; i < target_images.length; i++)
		target_images[i].setAttribute("src", response.target_state);
	let top_state = "";
	if(response.top_states.length)
		top_state = response.top_states[0];
	document.getElementById("top_1").setAttribute("src", top_state);

	// Set the current utterance
	let utterance_spans = document.getElementsByClassName("utterance");
	for(let i = 0; i < utterance_spans.length; i++)
		utterance_spans[i].textContent = state.last_utterance;

	if(is_current_program_slide(slide_i))
	{
		// Add the latest codeline to the list of codelines
		document.getElementById("codelines").innerHTML = "";
		document.getElementById("codelines").appendChild(code_to_html(response.codeline));
	}

	if(is_top_program_slide(slide_i))
	{
		// Add the current change to the top1 area
		document.getElementById("top_1_codeline").innerHTML = "";
		document.getElementById("top_1_codeline").appendChild(code_to_html(response.top_states_changes[0]));
	}

	if(is_disambiguation_slide(slide_i))
	{
		// Set the top transforms
		document.getElementById("top_states").innerHTML = "";
		let imagedivs = [];
		for(let i = 0; i < response.top_states.length; i++)
		{
			// Build HTML structures
			let imageimg = document.createElement("img");
			let overlay = document.createElement("div");
			let image = document.createElement("div");
			let message = document.createElement("div");
			let change = document.createElement("div");
			let score = document.createElement("div");
			overlay.innerHTML = "&checkmark;";
			overlay.classList.add("gallery-overlay");
			message.classList.add("image-caption");
			change.classList.add("codelinediv");
			score.classList.add("codelinediv");
			image.classList.add("gallery-container");
			message.appendChild(change);
			message.appendChild(score);
			image.appendChild(imageimg);
			image.appendChild(overlay);
			image.appendChild(message);

			// Set attributes
			change.appendChild(code_to_html(response.top_states_changes[i]));
			score.appendChild(code_to_html(response.top_states_scores[i]));
			imageimg.src = response.top_states[i];
			image.setAttribute("id", i);
			image.setAttribute("selected", 0);
			image.classList.add("image-unselected");
			image.onclick = function() {
				if(remaining_tasks > 0) return;
				// On click, toggle selection status
				let image_selected = image.getAttribute("selected") == 1;
				if(image_selected)
				{
					let newclasses = [];
					for(const oldclass of image.classList)
						if(oldclass != "image-selected")
							newclasses.push(oldclass);
					image.classList = newclasses;
					image.classList.add("image-unselected");
					image.setAttribute("selected", 0);
				}
				else
				{
					let newclasses = [];
					for(const oldclass of image.classList)
						if(oldclass != "image-unselected")
							newclasses.push(oldclass);
					image.classList = newclasses;
					image.classList.add("image-selected");
					image.setAttribute("selected", 1);
				}
				update_gallery_state();
			};
			imagedivs.push(image);
		}

		let image_row = document.createElement("div");
		let image_row_length = 0;
		for(let i = 0; i < imagedivs.length; i++)
		{
			image_row.appendChild(imagedivs[i]);
			image_row_length = image_row_length + 1;
			if(image_row_length == images_per_row)
			{
				image_row.classList.add("image-gallery-row");
				document.getElementById("top_states").appendChild(image_row);
				image_row = document.createElement("div");
				image_row_length = 0;
			}
		}
		update_gallery_state();
	}

	if(is_summary_slide(slide_i))
	{
		let summaries = document.getElementsByClassName("summary");
		for(let i = 0; i < summaries.length; i++)
		{
			let summary = summaries[i];
			summary.innerHTML = "";
			for(let j = 0; j < response.summary.length; j++)
			{
				let metric = response.summary[j];
				let metric_element = document.createElement("p");
				metric_element.innerHTML = metric;
				summary.appendChild(metric_element);
			}
		}
	}


	// Apply syntax highlight to all code blocks
	document.querySelectorAll('pre code').forEach((block) => {
		hljs.highlightBlock(block);
	});


	// If we received a non-empty alert, display it
	if(response._alert.length > 0)
		alert(response._alert);

	// Set the current slide
	currentSlide(slide_i);
}

/*
 * This function resets the local state.
 */
function reset()
{
	let draggable_div = document.getElementById("target_plot_constant_div");
	draggable_div.style.visibility = "visible";

	state.slide_i = 0;
	state.accepted_transforms = [];
	state.rejected_transforms = [];
	state.current_state = get_starting_program(get_current_puzzle_id());
	state.target_state = get_target_program(get_current_puzzle_id());
	state.top_states = [];
	state.sorted_transforms = [];
	state.sorted_transforms_scores = [];
	state.sorted_transforms_codes = [];
	with_program_image_src(state.current_state, (current_program_img) => {
	with_program_image_src(state.target_state, (target_program_img) => {
	with_program_code(state.current_state, (program_code) => {
		state.current_state_img = current_program_img;
		state.target_state_img = target_program_img;
		state.codeline = program_code;
		updateContent(state);
	});});});
}

/* Sends the sentence in the text input if it is not the empty string. */
function send_sentence() {
	// Clean sentence
	let sentence = document.getElementById("sentence").value;
	if(sentence.trim() === "") return;
	let sentence_input = document.getElementById("sentence");
	sentence_input.value = "";
	sentence_input.focus();

	// Update state
	state.last_utterance = sentence;
	state.accepted_transforms = [];
	state.rejected_transforms = [];
	state.slide_i += 1;

	negotiation_step = 0;

	add_to_csv(
		user_id,
		solved_puzzles,
		get_current_puzzle_id(),
		transformations_applied_in_current_puzzle,
		state.last_utterance,
		negotiation_step,
		state.sorted_transforms,
		get_local_bindings_size(),
		USER_UTTERANCE,
		state.accepted_transforms,
		state.rejected_transforms
	);

	// Sort and render content
	sort_state_inplace_and_update_content(state);
}

function accept_top_transform() {
	// First add the new binding.
	add_binding(
		state.last_utterance,
		state.sorted_transforms[0],
		state.sorted_transforms_codes[0],
		user_id,
	);
	add_to_csv(
		user_id,
		solved_puzzles,
		get_current_puzzle_id(),
		transformations_applied_in_current_puzzle,
		state.last_utterance,
		negotiation_step,
		state.sorted_transforms,
		get_local_bindings_size(),
		ACCEPT_FIRST_TRANSFORM,
		state.accepted_transforms,
		state.rejected_transforms
	);

	// Then we ask if we completed the puzzle
	state.slide_i = 0;
	with_program_equals(state.top_states[0], state.target_state, (solved_puzzle) => {
		if(solved_puzzle)
		{
			add_completed_puzzle(
				get_current_puzzle_id(),
				solved_puzzles,
				user_id,
				get_starting_program(get_current_puzzle_id()),
				get_target_program(get_current_puzzle_id()),
				get_local_bindings_size(),
				scoring_queries_in_current_puzzle
			);
			transformations_applied_in_current_puzzle = 0;
			scoring_queries_in_current_puzzle = 0;
			if(solved_puzzles < puzzle_number-1)
			{
				// If the puzzle is not the last one, set up everything to continue.
				// This is the only place where local bindings are updated.
				solved_puzzles += 1;
				state.current_state = get_starting_program(get_current_puzzle_id());
				state.target_state = get_target_program(get_current_puzzle_id());
				with_program_image_src(state.current_state, (current_program_img) => {
				with_program_image_src(state.target_state, (target_program_img) => {
				with_updated_bindings((bindings) => {
					state.current_state_img = current_program_img;
					state.target_state_img = target_program_img;
					local_bindings = bindings;
					updateContent(state);
				});});});
				alert("Good job! Only " + (puzzle_number-solved_puzzles) + " to go.");
			}
			else
			{
				// Otherwise (the puzzle is the last one), we simply show the final
				// screen.
				state.slide_i = 4;
				state.summary = ["SURVEY CODE:" + SURVEY_CODE];
				updateContent(state);
				alert("Good job! You finished. Here's your code: " + SURVEY_CODE);
			}
		}
		else
		{
			// If we did not complete the puzzle, simply update the content to
			// reflect the application of the transform.
			state.current_state = state.top_states[0];
			state.current_state_img = state.top_states_imgs[0];
			with_program_code(state.current_state, (program_code) => {
				state.codeline = program_code;
				updateContent(state);
			});
		}
	});
}

/* Sends the click top_1 button. */
function send_top_1(accepted_top_1) {
	let solved_puzzle = false;

	if(accepted_top_1)
	{
		// The user said "Yes, this is what I want."
		transformations_applied_in_current_puzzle += 1;
		accept_top_transform();
	}
	else
	{
		// The user said "No, this is not what I want."
		state.slide_i += 1;
		if(negotiation_step == 0)
			negotiation_step += 1;
		updateContent(state);
	}
}

/* Sends the "this is it" choice (user gallery choice). */
function send_force_top(transform_index) {
	// Go back to second slide but put the new transform at the beginning of
	// the sorted transform list.
	state.slide_i = 1;
	state.top_states.unshift(state.top_states[transform_index]);
	state.sorted_transforms.unshift(state.sorted_transforms[transform_index]);
	state.sorted_transforms_scores.unshift(
			state.sorted_transforms_scores[transform_index]
		);
	state.top_states_imgs.unshift(
		state.top_states_imgs[transform_index]
	);
	state.sorted_transforms_codes.unshift(
		state.sorted_transforms_codes[transform_index]
	);
	add_to_csv(
		user_id,
		solved_puzzles,
		get_current_puzzle_id(),
		transformations_applied_in_current_puzzle,
		state.last_utterance,
		negotiation_step,
		state.sorted_transforms,
		get_local_bindings_size(),
		THIS_IS_IT_CLICK,
		state.accepted_transforms,
		state.rejected_transforms
	);
	updateContent(state);
}

/* Sends the click top_1 button. */
function send_give_up() {
	// The user clicked "Cancel". We simply return them to the "utterance" screen
	// to begin the round.
	state.slide_i = 0;
	updateContent(state);
}

function send_accepted_rejected() {
	// The user said "More like these/None of these".
	// Get selected and unselected images
	negotiation_step += 1;
	for(let i of get_selected_images())
		state.accepted_transforms.push(state.sorted_transforms[i]);
	for(let i of get_unselected_images())
		state.rejected_transforms.push(state.sorted_transforms[i]);

	// Unselect all selected images
	let selected_images = document.getElementsByClassName("image-selected");
	for(let image of selected_images)
	{
		image.classList.remove("image-selected");
		image.setAttribute("selected", 0);
	}

	// Re-sort
	sort_state_inplace_and_update_content(state);
}

/* Sets up the behaviour of click-able images and buttons. */
function initial_setup() {
	// Set up the sentence button
	let sentence_button = document.getElementById("sentence-button");
	sentence_button.onclick = function () {
		if(remaining_tasks > 0) return;
		send_sentence();
	}

	// Set up the sentence input
	let sentence_input = document.getElementById("sentence");
	sentence_input.value = "";
	sentence_input.focus();
	sentence_input.addEventListener("keyup", function(event) {
		if (event.key === 'Enter') {
			event.preventDefault();
			sentence_button.click();
		}
	});

	// Set the top_1 click-able buttons
	let top_1_approval_button = document.getElementById("top_1_approval_button");
	let top_1_negation_button = document.getElementById("top_1_negation_button");
	top_1_approval_button.onclick = function() {
		if(remaining_tasks > 0) return;
		send_top_1(true);
	};
	top_1_negation_button.onclick = function() {
		if(remaining_tasks > 0) return;
		send_top_1(false);
	};

	// Set the fake slide button
	document.getElementById("fake_slide_button1").onclick = function() {
		if(remaining_tasks > 0) return;
		fake1_passed = true;
		plusSlides(1);
		reset();
		var float_caption = document.getElementById("float_caption");
		if(float_caption)
			float_caption.style.display = "block";
	};
	document.getElementById("fake_slide_button2").onclick = function() {
		if(remaining_tasks > 0) return;
		fake2_passed = true;
		plusSlides(1);
	};

	// Set up the resample button
	document.getElementById("resample_button").onclick = function() {
		if(remaining_tasks > 0) return;
		send_accepted_rejected();
	};

	// Set up the "this is it" button
	document.getElementById("top_1_button").onclick = function() {
		if(remaining_tasks > 0) return;
		let selected = get_selected_images();
		if(selected.length != 1)
			return;
		send_force_top(selected[0]);
	};
	update_gallery_state();

	// Set up the give up button
	let give_up_buttons = document.getElementsByClassName("give_up_button");
	for(let i = 0; i < give_up_buttons.length; i++)
	{
		let give_up_button = give_up_buttons[i];
		give_up_button.onclick = function() {
			if(remaining_tasks > 0) return;
			send_give_up();
		};
	}

	// Set up the reload button
	let reload_buttons = document.getElementsByClassName("reload_button");
	for(let i = 0; i < reload_buttons.length; i++)
	{
		let reload_button = reload_buttons[i];
		reload_button.onclick = function() {
			if(remaining_tasks > 0) return;
			let problems_sentence = document.getElementById("problems").value;
			let strategies_sentence = document.getElementById("strategies").value;
			add_comment(
				problems_sentence,
				strategies_sentence,
				user_id
			);
			sentence.disabled = true;
			reload_button.disabled = true;
			reload_button.value = "thanks!";
		};
	}

	let project_names = document.getElementsByClassName("project_name");
	for(let i = 0; i < project_names.length; i++)
		project_names[i].textContent = project_name_str;
}

// Format the bar and the slideshow immediately
currentSlide(1);
update_bar();
let start_button = document.getElementById("fake_slide_button1");
start_button.value = "Loading... Wait please.";
start_button.disabled = true;
let draggable_div = document.getElementById("target_plot_constant_div");
draggable_div.style.visibility = "hidden";

document.addEventListener('DOMContentLoaded', function() {
	// As soon as the page loads, request the server for critical information
	// to set as local variables.
	// (This would be prettier with promises...)
	with_updated_bindings((bindings) => {
	with_puzzle_number((puzzle_number_) => {
	with_target_programs(puzzle_number_, (target_programs_) => {
	with_starting_programs(puzzle_number_, (starting_programs_) => {
	with_transform_number((transform_number_) => {
	with_transform_codes(transform_number_, (transform_codes_) => {
		local_bindings = bindings;
		puzzle_number = puzzle_number_;
		transform_number = transform_number_;
		transform_codes = transform_codes_;
		starting_programs = starting_programs_;
		target_programs = target_programs_;
		start_button.value = "Start!";
		start_button.disabled = false;
		user_id = get_user_id();
		initial_setup();
	});});});});});});
}, false);
