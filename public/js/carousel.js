// Full credit:
// https://codepen.io/fredericrous/pen/xxVXJYX
//
const slider = document.querySelector('.carousel');
const next_buttons = document.querySelectorAll('.carousel_next');
const previous_buttons = document.querySelectorAll('.carousel_previous');
let carousel_elements = slider.querySelectorAll("li");
let active_ul = 0;
let isDown = false;
let startX;
let scrollLeft;
let QUIZ1_SLIDE_I = 5;
let QUIZ2_SLIDE_I = 6;

// This works but we don't need it
/*
slider.addEventListener('mousedown', e => {
  isDown = true;
  slider.classList.add('active');
  startX = e.pageX - slider.offsetLeft;
  scrollLeft = slider.scrollLeft;
});
slider.addEventListener('mouseleave', _ => {
  isDown = false;
  slider.classList.remove('active');
});
slider.addEventListener('mouseup', _ => {
  isDown = false;
  slider.classList.remove('active');
});
slider.addEventListener('mousemove', e => {
  if (!isDown) return;
  e.preventDefault();
  const x = e.pageX - slider.offsetLeft;
  const SCROLL_SPEED = 3;
  const walk = (x - startX) * SCROLL_SPEED;
  slider.scrollLeft = scrollLeft - walk;
});
*/

function quiz_answer(radio_name)
{
	var answers = document.getElementsByName(radio_name);

	for(var i = 0; i < answers.length; i++) {
		if(answers[i].checked)
		{
			return answers[i].value;
		}
	}
	return null;
}

function is_quiz1_done()
{
	return quiz_answer("task") == "Modify the plot so that it matches the given target";
}

function is_quiz2_done()
{
	return quiz_answer("how_to_talk") == "Instruct Robbie using specific language that changes the plot one step at a time";
}

for(next_button of next_buttons)
{
	next_button.addEventListener("click", e => {
		if(active_ul < carousel_elements.length-1)
		{
			carousel_elements[active_ul].classList.remove('activeli');
			active_ul = active_ul + 1;
			if(active_ul > QUIZ1_SLIDE_I && !is_quiz1_done())
			{
				alert("Check your answer!");
				active_ul = QUIZ1_SLIDE_I;
			}
			if(active_ul > QUIZ2_SLIDE_I && !is_quiz2_done())
			{
				alert("Check your answer!");
				active_ul = QUIZ2_SLIDE_I;
			}
			carousel_elements[active_ul].scrollIntoView({inline: "center", behavior: "smooth"});
			carousel_elements[active_ul].classList.add('activeli');
		}
	});
}

for(previous_button of previous_buttons)
{
	previous_button.addEventListener("click", e => {
		if(active_ul > 0)
		{
			carousel_elements[active_ul].classList.remove('activeli');
			active_ul = active_ul - 1;
			carousel_elements[active_ul].scrollIntoView({inline: "center", behavior: "smooth"});
			carousel_elements[active_ul].classList.add('activeli');
		}
	});
}

// Scroll into the first element when loaded
if(carousel_elements.length > 0)
	carousel_elements[0].scrollIntoView({inline: "center", behavior: "smooth"});
