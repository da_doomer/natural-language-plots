<!-- We use Google Firebase -->
const firebaseConfig = {
  apiKey: "AIzaSyBN0TBtgV1lTGg35kzVG50gcTKtfDo2TK0",
  authDomain: "nl-plots.firebaseapp.com",
  databaseURL: "https://nl-plots-default-rtdb.firebaseio.com",
  projectId: "nl-plots",
  storageBucket: "nl-plots.appspot.com",
  messagingSenderId: "876971234361",
  appId: "1:876971234361:web:3ee60f8bc9ec7e18e7a9e8"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
var database = firebase.database();

/* Returns a dictionary with this minimal structure:
 *
 *
 * {
 *   "binding_id":{
 *      "utterance": "a sentence in English",
 *      "transform": {
 *         "key": "color",
 *         "value": "green",
 *         "relative": false,
 *      },
 *   },
 *   "binding_id2": {
 *      ...
 *   },
 *   ...
 * }
 *
 */
var local_bindings = {};
function with_updated_bindings(callback) {
	database.ref("bindings").once("value", ((snapshot) => {
		bindings = snapshot.val();
		if(bindings === null)
			bindings = {};
		callback(bindings);
	}));
}


function add_binding(sentence, transform, transform_code, user_id) {
	data = {
		utterance: sentence,
		transform: transform,
		transform_code: transform_code,
		date: Date(),
		user_id: user_id
	};
	database.ref("bindings").push(data);
}

function add_completed_puzzle(
		puzzle_id,
		local_puzzle_number,
		user_id,
		starting_program,
		target_program,
		local_bindings_size,
		server_scoring_queries,/* times the server was used to score transforms in this puzzle */
		) {
	data = {
		puzzle_id: puzzle_id,
		local_puzzle_number: local_puzzle_number,
		user_id: user_id,
		date: Date(),
		starting_program: starting_program,
		target_program: target_program,
		local_bindings_size: local_bindings_size,
		server_scoring_queries: server_scoring_queries
	};
	database.ref("puzzle_completions").push(data);
}

function add_comment(
		problems,
		strategies
		) {
	data = {
		problems: problems,
		strategies: strategies,
		user_id: user_id,
		date: Date()
	};
	database.ref("comments").push(data);
}

function add_to_csv(
	user_id, /* we use Prolific ID strings */
	trial_id, /* current number of solved puzzles */
	puzzle_id, /* current puzzle id */
	transformation_number, /* number of transformations applied in the current puzzle */
	utterance, /* current last user utterance */
	negotiation_step, /* current negotiation round number. Resets on each transform application */
	transformation_order, /* current sorted list of ALL transformations. Updated after user utterance */
	local_bindings_size, /* current local bindings size */
	operation_id, /* operation ID */
	accepted_transforms, /* accepted transforms */
	rejected_transforms /* rejected transforms */
) {
	data = {
		user_id: user_id,
		trial_id: trial_id,
		puzzle_id: puzzle_id,
		transformation_number: transformation_number,
		utterance: utterance,
		negotiation_step: negotiation_step,
		transformation_order: transformation_order,
		local_bindings_size: local_bindings_size,
		operation_id: operation_id,
		accepted_transforms: accepted_transforms,
		rejected_transforms: rejected_transforms,
		date: Date()
	};
	database.ref("csv_rows").push(data);
}
