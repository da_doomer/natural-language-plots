// Source: https://www.w3schools.com/howto/howto_js_draggable.asp
function makeDraggable(elmnt) {
	var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
	var moved_eps = 10;
	var visibility_status = false;
	var moved = false;
	if (document.getElementById(elmnt.id + "header")) {
		// if present, the header is where you move the DIV from:
		document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
		if (document.getElementById(elmnt.id + "content")) {
			document.getElementById(elmnt.id + "content").style.display = "none";
			document.getElementById(elmnt.id + "header").onclick = toggleContentVisibility;
		}
	} else {
		// otherwise, move the DIV from anywhere inside the DIV:
		elmnt.onmousedown = dragMouseDown;
	}

	function dragMouseDown(e) {
		e = e || window.event;
		e.preventDefault();
		// get the mouse cursor position at startup:
		pos3 = e.clientX;
		pos4 = e.clientY;
		moved = false;
		document.onmouseup = closeDragElement;
		// call a function whenever the cursor moves:
		document.onmousemove = elementDrag;
	}

	function elementDrag(e) {
		e = e || window.event;
		e.preventDefault();
		// calculate the new cursor position:
		pos1 = pos3 - e.clientX;
		pos2 = pos4 - e.clientY;
		pos3 = e.clientX;
		pos4 = e.clientY;
		// set the element's new position:
		elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
		elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
		if(Math.abs(pos1) > moved_eps || Math.abs(pos2) > moved_eps)
			moved = true;
	}

	function closeDragElement() {
		// stop moving when mouse button is released:
		document.onmouseup = null;
		document.onmousemove = null;
	}

	function toggleContentVisibility() {
		if(moved) return;
		let content = document.getElementById(elmnt.id + "content");
		visibility_status = !visibility_status;
		if(visibility_status)
			content.style.display = "block";
		else
			content.style.display = "none";
	}
}
