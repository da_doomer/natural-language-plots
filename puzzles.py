"""Puzzle generation."""
# Local imports
from plots import PlotProgram
from plots import PlotProgramTransform
from hyperparameters import ExperimentHyperparameters
from bindings import Bindings
from models import DimensionalityReducer

# Native imports
from typing import Tuple
from typing import List
from typing import Set
from pathlib import Path
import json
import random
import copy

# External imports
import jsons
import joblib


Puzzle = Tuple[PlotProgram, PlotProgram]


def transform_changes_program(
        program: PlotProgram,
        transform: PlotProgramTransform
        ) -> bool:
    new_program = transform.transformed(program)
    diff = PlotProgram.diff_pixels(program, new_program)
    return set(diff.flatten()) != set([0])


def get_non_redundant_transform(
        program: PlotProgram, tbank: List[PlotProgramTransform]
        ) -> PlotProgramTransform:
    shuffled_tbank = copy.deepcopy(tbank)
    random.shuffle(shuffled_tbank)
    for transform in shuffled_tbank:
        if transform_changes_program(program, transform):
            return transform
    raise ValueError("Every transform is redundant with respect to this program.")


def generate_puzzle(
        tbank: List[PlotProgramTransform],
        difficulty: int,
        ) -> Puzzle:
    # Start with a proto program
    proto_program = PlotProgram()

    # Find a starting program with no redundant attributes
    starting_program = proto_program
    for _ in range(difficulty):
        transform = get_non_redundant_transform(starting_program, tbank)
        starting_program = transform.transformed(starting_program)

    # Find a target program with no redundant attributes
    transform = get_non_redundant_transform(starting_program, tbank)
    target_program = transform.transformed(starting_program)
    for _ in range(difficulty):
        transform = get_non_redundant_transform(target_program, tbank)
        target_program = transform.transformed(target_program)
    return (starting_program, target_program)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Generate an experiment")
    parser.add_argument(
            '--basis_file',
            help='Transform basis.',
            type=Path,
            required=True,
        )
    parser.add_argument(
            '--transform_bank',
            help='Transform bank.',
            type=Path,
            required=True,
        )
    parser.add_argument(
            '--output_dir',
            help='Output experiment directory.',
            type=Path,
            required=True,
        )
    parser.add_argument(
            '--puzzle_number',
            help='Number of puzzles for each batch.',
            type=int,
            required=True,
        )
    parser.add_argument(
            '--difficulty',
            help='Puzzle difficulty.',
            type=int,
            required=True,
        )
    parser.add_argument(
            '--batch_size',
            help='Number of users for each batch.',
            type=int,
            required=True,
        )
    args = parser.parse_args()
    args.output_dir.mkdir(exist_ok=True)
    print(f"Mkdir {args.output_dir}")

    # Load program basis
    with open(args.basis_file, "rt") as basis_file:
        basis_json = json.load(basis_file)
        basis = [
                PlotProgram(kwargs=kwargs)
                for kwargs in basis_json
            ]
        print("Loaded {}".format(str(args.basis_file)))

    # Open transform bank
    tbank = set()
    with open(args.transform_bank, "rt") as f:
        tbank_str = "\n".join(f)
    tbank = set(jsons.loads(
            tbank_str,
            cls=List[PlotProgramTransform],
        ))
    print(f"Loaded {args.transform_bank}")

    # Clean the tbank for the current set of allowed plot attributes
    for t in tuple(tbank):
        if t.key not in PlotProgram.default_kwargs:
            tbank.remove(t)
    tbank = list(tbank)

    # Generate puzzles
    puzzles = [
            generate_puzzle(tbank, args.difficulty)
            for _ in range(args.puzzle_number)
        ]

    def serialize(obj, path: Path):
        json_dict = jsons.dump(obj, strip_privates=True)
        json_str = json.dumps(json_dict, indent=4)
        with open(path, "wt") as f:
            f.write(json_str)
        print(f"Wrote {path}")

    # Serialize bindings
    bindings = Bindings()
    bindings_path = args.output_dir/"bindings.json"
    serialize(bindings, bindings_path)

    # Serialize transform bank
    transform_bank_path = args.output_dir/"transform_bank.json"
    serialize(tbank, transform_bank_path)

    # Serialize puzzles
    puzzles_paths = list()
    for i, (starting_program, target_program) in enumerate(puzzles):
        starting_program_path = args.output_dir/f"starting_program_{i}.json"
        target_program_path = args.output_dir/f"target_program_{i}.json"
        serialize(starting_program.kwargs, starting_program_path)
        serialize(target_program.kwargs, target_program_path)
        puzzles_paths.append((starting_program_path, target_program_path))

    # Serialize the experiment
    experiment = ExperimentHyperparameters(
            bindings=bindings_path,
            transform_bank=transform_bank_path,
            tasks=puzzles_paths,
            batch_size=args.batch_size,
        )
    experiment_path = args.output_dir/"hyperparameters.json"
    serialize(experiment, experiment_path)
