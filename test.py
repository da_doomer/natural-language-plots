"""Load-test the server."""
import requests
import json
from pathlib import Path
import random
from plots import PlotProgram
import time
import copy

root = "https://plots.iamleo.space"

test_number = 10
word2vec_json = Path("data/wordvecs1000.js")
# Load word2vec file
with open(word2vec_json) as word2vec_json_file:
    word2vec = json.load(word2vec_json_file)
    print(f"Loaded {word2vec_json}")


# Test transform number
url = f"{root}/transform_number"
response = requests.get(url)
t_number = int(response.text)
print(f"The server says there are {t_number} transforms")


# Test puzzle number
url = f"{root}/puzzle_number"
response = requests.get(url)
p_number = int(response.text)
print(f"The server says there are {p_number} puzzles")

print("Heavy load testing ahead.")
time.sleep(2.0)


# Test puzzle starting programs
for i in range(test_number):
    pid = random.choice(range(p_number))
    url = f"{root}/puzzles/{pid}/starting_program"
    response = requests.get(url)
    starting_program = PlotProgram(json.loads(response.text))
    print(f"{url} --- {starting_program}")


# Test puzzle target programs
for i in range(test_number):
    pid = random.choice(range(p_number))
    url = f"{root}/puzzles/{pid}/target_program"
    response = requests.get(url)
    target_program = PlotProgram(json.loads(response.text))
    print(f"{url} --- {target_program}")



# Test program plotting
for i in range(test_number):
    url = f"{root}/plot_program"
    program = PlotProgram.init_random()
    data = program.kwargs
    response = requests.post(url, json=data)
    assert Path(response.text).exists()
    print(f"{url} --- {program} {response.text}")


# Test program code
for i in range(test_number):
    url = f"{root}/program_code"
    program = PlotProgram.init_random()
    data = program.kwargs
    response = requests.post(url, json=data)
    print(f"{url} --- {program} {response.text}")


# Test transformation scores
words = tuple(word2vec.keys())
for i in range(test_number):
    def random_utterance():
        return  " ".join(random.choices(words, k=random.choice(range(1,10))))
    utterance = random_utterance()
    bindings = {
            f"binding_{j}":
            {
                "utterance": random_utterance(),
                "transform": random.choice(range(t_number)),
            }
            for j in range(10)
        }
    url = f"{root}/transform_scores/{utterance}"
    data = {
            "accepted": random.choices(range(t_number)),
            "rejected": random.choices(range(t_number)),
            "bindings": bindings,
        }
    response = requests.post(url, json=data)
    print(f"{url} --- {response.text}")
    scores = json.loads(response.text)


# Test transformed program
for i in range(test_number):
    tid = random.choice(range(t_number))
    url = f"{root}/transformed_program/{tid}"
    program = PlotProgram.init_random()
    data = program.kwargs
    response = requests.post(url, json=data)
    transformed_program = PlotProgram(json.loads(response.text))
    print(f"{url} {program} --- {transformed_program}")


# Test transform code
for i in range(test_number):
    tid = random.choice(range(t_number))
    url = f"{root}/transform_code/{tid}"
    response = requests.get(url)
    print(f"{url} --- {response.text}")


# Test program equality
for i in range(test_number):
    url = f"{root}/program_equality"
    program1 = PlotProgram.init_random()
    program2 = copy.deepcopy(program1)
    data = {"program1": program1.kwargs, "program2": program2.kwargs}
    response = requests.post(url, json=data)
    assert int(response.text) == 1
    response = requests.post(url, json=json.dumps(data))
    assert int(response.text) == 1
    print(f"{url} {program1} {program2} --- {response.text}")


# Test program inequality
inequality_possible_errors = 0
for i in range(test_number):
    url = f"{root}/program_equality"
    program1 = PlotProgram.init_random()
    program2 = PlotProgram.init_random()
    data = {"program1": program1.kwargs, "program2": program2.kwargs}
    response = requests.post(url, json=data)
    if int(response.text) != 0:
        inequality_possible_errors += 1
    assert inequality_possible_errors < test_number*0.01
    response = requests.post(url, json=json.dumps(data))
    if int(response.text) != 0:
        inequality_possible_errors += 1
    assert inequality_possible_errors < test_number*0.01
    print(f"{url} {program1} {program2} --- {response.text}")
