"""Defines utterance encoding logic."""
# Native imports
from functools import reduce
import math
import random
from typing import Dict
from typing import List
import string


def sanitize_str(word: str) -> str:
    # Lower case and remove extra whitespace
    word = word.lower().strip()

    # If its a number, return it as-is
    try:
        float(word)
        return word
    except ValueError:
        pass

    # If its not a number, remove all non alphabetic characters
    # where alphabet=[a-z], and turn it lower case
    allowed = string.ascii_letters + " "
    return "".join((
            letter
            for letter in word.lower().strip()
            if letter in allowed
        ))


def sumvecs(v1, v2):
    return [v1i+v2i for v1i, v2i in zip(v1, v2)]


def dot_product(v1, v2):
    return sum((v1i*v2i for v1i, v2i in zip(v1, v2)))


def cosine_similarity(v1, v2):
    zero = [0.0 for _ in v1]
    dotnorm = math.dist(v1, zero)*math.dist(v2, zero)
    dotprod = dot_product(v1, v2)
    return dotprod/dotnorm


def encode(utterance: str, word2vec: Dict[str, List[float]]):
    """Encodes utterance."""
    # If we do not have a word, map to a random vector. We don't map to the
    # zero vector as that may incorrectly lead to concluding two different
    # utterances are the same.
    for word in utterance.split():
        if word not in word2vec.keys():
            random_vec = random.choice(tuple(word2vec.values()))
            zero = [0 for _ in random_vec]
            word2vec[word] = [random.random() for _ in zero]
    encoded_words = [word2vec[word] for word in utterance.split()]
    return reduce(sumvecs, encoded_words)
